package computer.someoneelses.day4;

import io.vavr.collection.Array;
import io.vavr.collection.HashSet;

public class Passphrases {

  public static boolean isPasswordValid(String passphrase) {
    Array<String> words = Array.of(passphrase.split(" "));
    return HashSet.ofAll(words).length() == words.length();
  }

  public static int countValid(String passphrases) {
    return Array.of(passphrases.split("\n"))
        .map(Passphrases::isPasswordValid)
        .map(valid -> valid ? 1 : 0)
        .sum()
        .intValue();
  }
}
