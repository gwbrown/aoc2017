package computer.someoneelses.day3;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.Array;
import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import io.vavr.collection.Stream;

import java.net.Inet4Address;

public class SpiralMemory {
  public static int solve(int target) {
    if (target == 1) return 0;

    // Determine what order of spiral the target is on
    final Double sqrt = Math.sqrt(target);
    System.out.println("Sqrt is " + sqrt);

    double order;
    int xPos, yPos;
    if (isEven(Math.floor(sqrt))) {
      order = sqrt.intValue() - 1;
    } else if (isOdd(sqrt)) {
      order = sqrt.intValue() - 2;
    } else {
      order = sqrt.intValue();
    }
    System.out.println("Order is " + order);

    // Determine what side the target is on
    if (sqrt <= order + 0.5) {
      // right
      System.out.println("Target is on the right.");
      xPos = orderToDistance(new Double(order).intValue());
      Double reference = Math.pow(order, 2);
      System.out.println("Reference is " + reference);
      yPos = xPos - Math.abs((target - reference.intValue()));
    } else if (sqrt < order + 1) {
      // top
      System.out.println("Target is on the top.");
      yPos = orderToDistance(new Double(order).intValue());
      Double reference = Math.pow(order + 1, 2) + 1;
      System.out.println("Reference is " + reference);
      xPos = yPos - (reference.intValue() - target);
    } else if (sqrt < order + 1.5) {
      // left
      System.out.println("Target is on the left.");
      xPos = -orderToDistance(new Double(order).intValue());
      Double reference = Math.pow(order + 1, 2) + 1;
      System.out.println("Reference is " + reference);
      yPos = xPos + Math.abs((target - reference.intValue()));
    } else {
      // bottom
      System.out.println("Target is on the bottom.");
      yPos = -orderToDistance(new Double(order).intValue());
      Double reference = Math.pow(order + 2, 2);
      System.out.println("Reference is " + reference);

      xPos = yPos + Math.abs((reference.intValue() - target));
    }


    // Determine how far from the corner the target is

    // Now we have the point.

    System.out.println("Point is at (" + xPos + ", " + yPos + ")");
    return Math.abs(yPos) + Math.abs(xPos);
  }

  private static boolean isEven(double i) {
    return i % 2 == 0;
  }

  private static boolean isOdd(double i) {
    return i % 2 == 1;
  }

  private static int orderToDistance(int order) {
    return ((order - 1) / 2) + 1;
  }

  public static int biggestLargerThan(int target) {
    Stream<Integer> counts = Stream.from(1).flatMap(i -> Stream.of(i, i));
    Stream<DIRECTION> directions = Stream.of(DIRECTION.LEFT, DIRECTION.UP, DIRECTION.RIGHT, DIRECTION.DOWN).cycle();
    Stream<DIRECTION> steps = counts.zip(directions).flatMap(pair -> Stream.of(pair._2()).cycle(pair._1()));

    Tuple2<Integer, Integer> pointer = new Tuple2<>(0, 0);
    HashMap<Tuple2<Integer, Integer>, Integer> grid = generatePoint(HashMap.empty(), pointer);
    while (grid.get(pointer).getOrElse(0) < target) {
      pointer = nextPoint(pointer, steps.head());
      grid = generatePoint(grid, pointer);
      steps = steps.drop(1);
    }

    return grid.get(pointer).getOrNull();
  }

  private static HashMap<Tuple2<Integer, Integer>, Integer> generatePoint(HashMap<Tuple2<Integer, Integer>, Integer> grid,
                                                                         Tuple2<Integer, Integer> coordinate) {
    if (coordinate._1() == 0 && coordinate._2() == 0) return grid.put(coordinate, 1);
    Array<Tuple2<Integer, Integer>> coords = Array.of(
          coordinate.update1(coordinate._1()+1),
          coordinate.update2(coordinate._2()+1),
          coordinate.update1(coordinate._1()+1).update2(coordinate._2()+1),
          coordinate.update1(coordinate._1()-1),
          coordinate.update2(coordinate._2()-1),
          coordinate.update1(coordinate._1()-1).update2(coordinate._2()-1),
          coordinate.update1(coordinate._1()+1).update2(coordinate._2()-1),
          coordinate.update1(coordinate._1()-1).update2(coordinate._2()+1)
        );
    return grid.put(coordinate, coords.map(c -> grid.getOrElse(c, 0)).sum().intValue());
  }

  private static Tuple2<Integer, Integer> nextPoint(Tuple2<Integer, Integer> currentPoint, DIRECTION direction) {
    switch (direction) {
      case LEFT:
        return currentPoint.update1(currentPoint._1() + 1);
      case UP:
        return currentPoint.update2(currentPoint._2() + 1);
      case RIGHT:
        return currentPoint.update1(currentPoint._1() - 1);
      case DOWN:
        return currentPoint.update2(currentPoint._2() - 1);
    }
    throw new RuntimeException("Invalid direction, this can't happen.");
  }

  private enum DIRECTION {LEFT, UP, RIGHT, DOWN}
}
