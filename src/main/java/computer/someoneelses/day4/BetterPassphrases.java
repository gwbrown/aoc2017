package computer.someoneelses.day4;

import io.vavr.collection.Array;
import io.vavr.collection.HashSet;

import java.util.Arrays;

public class BetterPassphrases {

  public static boolean isPasswordValid(String passphrase) {
    Array<String> words = Array.of(passphrase.split(" "))
        .map(word -> {
          char[] chars = word.toCharArray();
          Arrays.sort(chars);
          return new String(chars);
        });
    return HashSet.ofAll(words).length() == words.length();
  }

  public static int countValid(String passphrases) {
    return Array.of(passphrases.split("\n"))
        .map(BetterPassphrases::isPasswordValid)
        .map(valid -> valid ? 1 : 0)
        .sum()
        .intValue();
  }
}
