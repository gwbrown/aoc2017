package computer.someoneelses.day2;

import io.vavr.Tuple2;
import io.vavr.collection.Array;
import io.vavr.collection.Traversable;
import io.vavr.control.Option;

import java.util.Objects;

import static sun.management.snmp.jvminstr.JvmThreadInstanceEntryImpl.ThreadStateMap.Byte1.other;

public class SpreadshetChecksum {
  public static int solve(String spreadsheet) {
    return Array.of(spreadsheet.split("\n"))
        .map(row -> Array
            .of(row.split("\t"))
            .map(Integer::parseInt))
        .map(row -> row.max().get() - row.min().get())
        .sum()
        .intValue();
  }

  public static int solve2(String spreadsheet) {
    return Array.of(spreadsheet.split("\n"))
        .map(row -> Array
            .of(row.split("\t"))
            .map(Integer::parseInt))
        .map(row -> row
            .flatMap(e -> row
                .filter(other -> e % other == 0 && !Objects.equals(e, other)).map(o -> new Tuple2<>(e, o))))
        .map(Array::head)
        .map(pair -> pair._1() / pair._2())
        .sum()
        .intValue();
  }

}
