package computer.someoneelses.day3;

import org.junit.Test;

import static org.junit.Assert.*;

public class SpiralMemoryTest {

  @Test public void Test1() {
    assertEquals(SpiralMemory.solve(1), 0);
  }

  @Test public void Test12() {
    assertEquals(SpiralMemory.solve(12), 3);
  }

  @Test public void Test3() {
    assertEquals(SpiralMemory.solve(3), 2);
  }

  @Test public void Test18() {
    assertEquals(SpiralMemory.solve(18), 3);
  }

  @Test public void Test42() {
    assertEquals(SpiralMemory.solve(42), 5);
  }

  @Test public void Test23() {
    assertEquals(SpiralMemory.solve(23), 2);
  }
  @Test public void Test35() {
    assertEquals(SpiralMemory.solve(35), 4);
  }

  @Test public void Test41() {
    assertEquals(SpiralMemory.solve(41), 4);
  }

  @Test public void Test38() {
    assertEquals(SpiralMemory.solve(38), 5);
  }

  @Test public void Test39() {
    assertEquals(SpiralMemory.solve(39), 4);
  }

  @Test public void Test1024() {
    assertEquals(SpiralMemory.solve(1024), 31);
  }

  @Test public void ProblemSolution() {
    assertEquals(SpiralMemory.solve(265149), 438);
  }

  @Test public void P2Test3() {
    assertEquals(SpiralMemory.biggestLargerThan(3), 4);
  }

  @Test public void P2Test145() {
    assertEquals(SpiralMemory.biggestLargerThan(143), 147);
  }

  @Test public void P2Test22() {
    assertEquals(SpiralMemory.biggestLargerThan(17), 23);
  }

  @Test public void P2ProblemSolution() {
    assertEquals(SpiralMemory.biggestLargerThan(265149), 266330);
  }
}