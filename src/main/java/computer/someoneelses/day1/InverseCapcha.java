package computer.someoneelses.day1;

import io.vavr.Tuple2;
import io.vavr.collection.Array;
import io.vavr.collection.Stream;

import java.util.Objects;

public class InverseCapcha {
  public static int solve(String capcha) {
    String rotatedCapcha = capcha + capcha.substring(0,1);

    Array<Integer> digits = Array
        .ofAll(rotatedCapcha.toCharArray())
        .map(c -> Character.digit(c, 10));
    return digits
        .zip(digits.drop(1))
        .filter(pair -> Objects.equals(pair._1(), pair._2()))
        .map(Tuple2::_1)
        .sum()
        .intValue();
  }

  public static int solve2(String capcha) {

    Array<Integer> digits = Array
        .ofAll(capcha.toCharArray())
        .map(c -> Character.digit(c, 10));
    int hopSize = digits.length() / 2;
    return digits
        .appendAll(digits)
        .dropRight(hopSize)
        .sliding(hopSize+1)
//        .peek(System.out::println)
        .map(slice -> Objects.equals(slice.head(), slice.last()) ? slice.head() : 0)
        .sum()
        .intValue();
  }
}
